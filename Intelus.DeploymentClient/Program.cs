﻿using System;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Intelus.DeploymentClient
{
    class Program
    {
        static void Main()
        {
            MainAsync().Wait();
        }

        public static async Task MainAsync()
        {
            string sampleQuery = "I have an issue with my credit card payment";

            // Call endpoint with your query
            var getResponse = await QueryEndpointGetAsync(sampleQuery);
            Console.WriteLine(getResponse);

            var postResponse = await QueryEndpointPostAsync(sampleQuery);
            Console.WriteLine(postResponse);
        }


        // <summary>
        // This function will send GET request to deployment endpoint (provided in appSettings file)
        // and use api key autehnticaion.
        // </summary>
        public static async Task<string> QueryEndpointGetAsync(string query)
        {
            string url = $"{ConfigurationManager.AppSettings["END_POINT_URL"]}/apikey";
            string secretKey = ConfigurationManager.AppSettings["INTELUS_SECRET_KEY"];

            var httpClient = new HttpClient
            {
                BaseAddress = new Uri(url)
            };

            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            httpClient.DefaultRequestHeaders.Add("X-API-Key", secretKey);

            var response = await httpClient.GetAsync($"?query={query}");
            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsStringAsync();
            }
            else
            {
                return $"Execution failed with error: {response.ToString()}";
            }
        }

        // <summary>
        // This function will send POST request to deployment endpoint (provided in appSettings file)
        // and use api key autehnticaion.
        // </summary>
        public static async Task<string> QueryEndpointPostAsync(string query)
        {
            var httpClient = new HttpClient();

            string url = $"{ConfigurationManager.AppSettings["END_POINT_URL"]}/apikey";
            string secretKey = ConfigurationManager.AppSettings["INTELUS_SECRET_KEY"];
            QueryRequest queryReq = new QueryRequest(query);

            StringContent content = new StringContent(
                JsonSerializer.Serialize(queryReq),
                Encoding.UTF8,
                "application/json"
            );

            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            httpClient.DefaultRequestHeaders.Add("X-API-Key", secretKey);

            var response = await httpClient.PostAsync(url, content);
            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsStringAsync();
            }
            else
            {
                return $"Execution failed with error: {response.ToString()}";
            }
        }
    }
}