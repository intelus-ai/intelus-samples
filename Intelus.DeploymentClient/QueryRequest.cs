﻿using System;
namespace Intelus.DeploymentClient
{
    public class QueryRequest
    {
        public QueryRequest(string query)
        {
            Query = query;
        }

        public string Query { get; set; }
    }
}
