# intelus.ai Samples

Intelus Samples is a C# library that contains executable projects for the following flows:

- Query runtime endpoints generated from intelus.ai system.

## Installation

- Clone samples repo that contains executable projects for each flow.

## Usage

- Update **app.config** file with your environment specific values.

```xml
<!--The deployment endpoint url generated from intelus.ai system-->
<add key="END_POINT_URL" value="--OVERRIDE THIS WITH ENVIRONMENT-SPECIFIC VALUE--" />
<!--The secret key to be used in api key authentication endpoint-->
<add key="INTELUS_SECRET_KEY" value="--OVERRIDE THIS WITH ENVIRONMENT-SPECIFIC VALUE--"/>
```

- Check **Program.cs** file in the sample project

```python
string sampleQuery = "I have an issue with my credit card payment";

// Call endpoint with your query
var response = await QueryEndpointApiKeyAsync(sampleQuery);
Console.WriteLine(response);
```
